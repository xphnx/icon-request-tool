![CI badge](https://gitlab.com/xphnx/icon-request-tool/badges/master/build.svg)

# Turtl - Icon Request Tool

With this app you can share (via email, or other protocols like Owncloud, NFC, Bluetooth, IM, etc...)
the data for adding the icons missing in your icon pack.
It just collects the app title, package name, activity name and the icon file of the apps you select.

## Preview

![turtl_screenshot2](/uploads/f8c0cb7fe75dca41c47f152af7cf2d28/turtl_screenshot2.png)

# Installation
<a href="https://f-droid.org/packages/org.xphnx.iconsubmit">
    <img src="https://f-droid.org/badge/get-it-on.png"
    alt="Get it on F-Droid" height="80">
</a>
<a href="https://gitlab.com/xphnx/icon-request-tool/-/jobs/artifacts/master/download?job=build">
    <img src="https://hike.in/images/hike5.0/apk.png"
    alt="Download the latest build" height="80">
</a>

## Permissions

    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

These permissions are needed to be able to create the zip files.

## License

[<img src="https://www.gnu.org/graphics/gplv3-127x51.png" />](https://www.gnu.org/licenses/gpl-3.0.html)
